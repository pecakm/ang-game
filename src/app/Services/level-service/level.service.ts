import { Injectable } from '@angular/core';
import { LevelInfo } from '../../Models/LevelInfo';
import { Position } from '../../Models/Position';
import { CONSTANTS } from '../../helpers/Constants';

@Injectable({
	providedIn: 'root'
})
export class LevelService {
	public levelInfo = new LevelInfo();

	constructor() { }

	loadLevelInfo(level: number) {
		if (level == 0) {
			this.levelInfo.level = 1;
			this.levelInfo.mapWidth = 13;
			this.levelInfo.mapHeight = 13;
			this.levelInfo.obstaclePositions = [
				new Position(3, 3)
			];
			this.levelInfo.exitPosition = new Position(1, 1);
			this.levelInfo.currentPosition = new Position(6, 6);
			this.levelInfo.disableMove = 0;
			this.levelInfo.oldPosition = 0;
			this.levelInfo.direction = CONSTANTS.UP_KEY;
		} else if (level == 1) {
			this.levelInfo.level = 2;
			this.levelInfo.mapWidth = 20;
			this.levelInfo.mapHeight = 25;
			this.levelInfo.obstaclePositions = [
				new Position(1, 1),
				new Position(4, 4),
				new Position(7, 7),
				new Position(9, 10)
			];
			this.levelInfo.exitPosition = new Position(11, 10);
			this.levelInfo.currentPosition = new Position(11, 10);
			this.levelInfo.disableMove = CONSTANTS.LEFT_KEY;
			this.levelInfo.oldPosition = this.levelInfo.currentPosition.x;
			this.levelInfo.direction = CONSTANTS.LEFT_KEY;
		} else if (level == 2) {
			this.levelInfo.level = 1;
			this.levelInfo.mapWidth = 13;
			this.levelInfo.mapHeight = 13;
			this.levelInfo.obstaclePositions = [
				new Position(3, 3)
			];
			this.levelInfo.exitPosition = new Position(1, 1);
			this.levelInfo.currentPosition = new Position(1, 1);
			this.levelInfo.disableMove = CONSTANTS.DOWN_KEY;
			this.levelInfo.oldPosition = this.levelInfo.currentPosition.y;
			this.levelInfo.direction = CONSTANTS.DOWN_KEY;
		}
	}

	getDisableMove(): number {
		return this.levelInfo.disableMove;
	}

	checkExit() {
		if (this.levelInfo.currentPosition.x == this.levelInfo.exitPosition.x &&
			this.levelInfo.currentPosition.y == this.levelInfo.exitPosition.y) {
			this.loadLevelInfo(this.levelInfo.level);
		}
	}
}
