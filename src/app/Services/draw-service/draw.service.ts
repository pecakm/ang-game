import { Injectable } from '@angular/core';
import { CONSTANTS } from '../../helpers/Constants';
import { LevelService } from '../level-service/level.service';

@Injectable({
	providedIn: 'root'
})
export class DrawService {
	public cx: CanvasRenderingContext2D;

	constructor(
		private levelService: LevelService
	) { }

	drawView() {
		this.cx.clearRect(0, 0, CONSTANTS.CANVAS_SIZE, CONSTANTS.CANVAS_SIZE);
		this.drawFields();
		this.drawObstacles();
		this.drawExit();
		this.drawCenter();
	}

	drawFields() {
		for (let y = 0; y < this.levelService.levelInfo.mapHeight; y++) {
			for (let x = 0; x < this.levelService.levelInfo.mapWidth; x++) {
				this.cx.fillStyle = CONSTANTS.BORDER_COLOR;
				this.cx.fillRect(this.returnFieldXPosition(x),
					this.returnFieldYPosition(y),
					CONSTANTS.FIELD_SIZE,
					CONSTANTS.FIELD_SIZE);
				this.cx.fillStyle = CONSTANTS.FIELD_COLOR;
				this.cx.fillRect(this.returnFieldXContentPosition(x),
					this.returnFieldYContentPosition(y),
					CONSTANTS.FIELD_SIZE - 2 * CONSTANTS.BORDER_SIZE,
					CONSTANTS.FIELD_SIZE - 2 * CONSTANTS.BORDER_SIZE);
			}
		}
	}

	returnFieldXPosition(x: number): number {
		return (x + CONSTANTS.VIEW_RANGE - this.levelService.levelInfo.currentPosition.x) * CONSTANTS.FIELD_SIZE;
	}

	returnFieldYPosition(y: number): number {
		return (y + CONSTANTS.VIEW_RANGE - this.levelService.levelInfo.currentPosition.y) * CONSTANTS.FIELD_SIZE;
	}

	returnFieldXContentPosition(x: number): number {
		return (x + CONSTANTS.VIEW_RANGE - this.levelService.levelInfo.currentPosition.x) * CONSTANTS.FIELD_SIZE + CONSTANTS.BORDER_SIZE;
	}

	returnFieldYContentPosition(y: number): number {
		return (y + CONSTANTS.VIEW_RANGE - this.levelService.levelInfo.currentPosition.y) * CONSTANTS.FIELD_SIZE + CONSTANTS.BORDER_SIZE;
	}

	drawObstacles() {
		if (this.levelService.levelInfo.obstaclePositions.length > 0) {
			for (let i = 0; i < this.levelService.levelInfo.obstaclePositions.length; i++) {
				this.cx.fillStyle = CONSTANTS.OBSTACLE_COLOR;
				this.cx.fillRect(this.returnObstacleXPosition(i),
					this.returnObstacleYPosition(i),
					CONSTANTS.FIELD_SIZE,
					CONSTANTS.FIELD_SIZE);
			}
		}
	}

	returnObstacleXPosition(i: number): number {
		return (CONSTANTS.VIEW_RANGE - this.levelService.levelInfo.currentPosition.x + this.levelService.levelInfo.obstaclePositions[i].x) * CONSTANTS.FIELD_SIZE;
	}

	returnObstacleYPosition(i: number): number {
		return (CONSTANTS.VIEW_RANGE - this.levelService.levelInfo.currentPosition.y + this.levelService.levelInfo.obstaclePositions[i].y) * CONSTANTS.FIELD_SIZE;
	}

	drawExit() {
		this.cx.fillStyle = CONSTANTS.EXIT_COLOR;
    	this.cx.fillRect(this.returnExitXPosition(),
        	this.returnExitYPosition(),
        	CONSTANTS.FIELD_SIZE,
        	CONSTANTS.FIELD_SIZE);
	}

	returnExitXPosition() {
		return (CONSTANTS.VIEW_RANGE - this.levelService.levelInfo.currentPosition.x + this.levelService.levelInfo.exitPosition.x) * CONSTANTS.FIELD_SIZE;
	}

	returnExitYPosition() {
		return (CONSTANTS.VIEW_RANGE - this.levelService.levelInfo.currentPosition.y + this.levelService.levelInfo.exitPosition.y) * CONSTANTS.FIELD_SIZE;
	}

	drawCenter() {
		this.cx.fillStyle = CONSTANTS.CENTER_COLOR;
		this.cx.fillRect(CONSTANTS.VIEW_RANGE * CONSTANTS.FIELD_SIZE,
			CONSTANTS.VIEW_RANGE * CONSTANTS.FIELD_SIZE,
			CONSTANTS.FIELD_SIZE,
			CONSTANTS.FIELD_SIZE);
	}

	drawMessage() {
		this.cx.fillStyle = CONSTANTS.MESSAGE_BACKGROUND_COLOR;
		this.cx.fillRect(0,
			CONSTANTS.CANVAS_SIZE - 3 * CONSTANTS.FIELD_SIZE,
			CONSTANTS.CANVAS_SIZE,
			3 * CONSTANTS.FIELD_SIZE);
		this.cx.fillStyle = CONSTANTS.MESSAGE_COLOR;
		this.cx.font = '40px Arial';
		this.cx.fillText('> Flipendo!',
			CONSTANTS.FIELD_SIZE,
			CONSTANTS.CANVAS_SIZE - 2 * CONSTANTS.FIELD_SIZE);
	}
}
