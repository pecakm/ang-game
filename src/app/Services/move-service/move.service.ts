import { Injectable } from '@angular/core';
import { CONSTANTS } from '../../helpers/Constants';
import { LevelInfo } from '../../Models/LevelInfo';
import { DrawService } from '../draw-service/draw.service';
import { LevelService } from '../level-service/level.service';

@Injectable({
	providedIn: 'root'
})
export class MoveService {

	constructor(
		private drawService: DrawService,
		private levelService: LevelService
	) { }

	continueMove() {
		switch (this.levelService.levelInfo.disableMove) {
			case CONSTANTS.LEFT_KEY:
				this.moveLeft();
				break;

			case CONSTANTS.UP_KEY:
				this.moveUp();
				break;

			case CONSTANTS.RIGHT_KEY:
				this.moveRight();
				break;

			case CONSTANTS.DOWN_KEY:
				this.moveDown();
				break;
		}
	}

	moveLeft() {
		this.levelService.levelInfo.currentPosition.x = Math.round((this.levelService.levelInfo.currentPosition.x - CONSTANTS.SPEED) * 100) / 100;

    	if (this.levelService.levelInfo.oldPosition - this.levelService.levelInfo.currentPosition.x >= 1) {
        	this.levelService.levelInfo.currentPosition.x = this.levelService.levelInfo.oldPosition - 1;
        	this.finishMove();
    	}
	}

	finishMove() {
		this.levelService.levelInfo.disableMove = 0;
        this.levelService.levelInfo.oldPosition = 0;
	}

	moveUp() {
		this.levelService.levelInfo.currentPosition.y = Math.round((this.levelService.levelInfo.currentPosition.y - CONSTANTS.SPEED) * 100) / 100;

    	if (this.levelService.levelInfo.oldPosition - this.levelService.levelInfo.currentPosition.y >= 1) {
        	this.levelService.levelInfo.currentPosition.y = this.levelService.levelInfo.oldPosition - 1;
        	this.finishMove();
    	}
	}

	moveRight() {
		this.levelService.levelInfo.currentPosition.x = Math.round((this.levelService.levelInfo.currentPosition.x + CONSTANTS.SPEED) * 100) / 100;

    	if (this.levelService.levelInfo.currentPosition.x - this.levelService.levelInfo.oldPosition >= 1) {
        	this.levelService.levelInfo.currentPosition.x = this.levelService.levelInfo.oldPosition + 1;
        	this.finishMove();
    	}
	}

	moveDown() {
		this.levelService.levelInfo.currentPosition.y = Math.round((this.levelService.levelInfo.currentPosition.y + CONSTANTS.SPEED) * 10) / 10;
	
		if (this.levelService.levelInfo.currentPosition.y - this.levelService.levelInfo.oldPosition >= 1) {
			this.levelService.levelInfo.currentPosition.y = this.levelService.levelInfo.oldPosition + 1;
			this.finishMove();
		}
	}

	startLeftMove() {
		if (this.levelService.levelInfo.currentPosition.x > 0 &&
			!this.isObstacle(CONSTANTS.LEFT_KEY)) {
			this.levelService.levelInfo.disableMove = CONSTANTS.LEFT_KEY;
			this.levelService.levelInfo.oldPosition = this.levelService.levelInfo.currentPosition.x;
			this.moveLeft();
		}
	}

	isObstacle(key: number): boolean {
		if (this.levelService.levelInfo.obstaclePositions.length > 0) {
			switch (key) {
				case CONSTANTS.LEFT_KEY:
				for (let obstacle of this.levelService.levelInfo.obstaclePositions) {
					if (obstacle.y == this.levelService.levelInfo.currentPosition.y &&
						obstacle.x + 1 == this.levelService.levelInfo.currentPosition.x) {
						return true;
					}
				}
				return false;
	
				case CONSTANTS.UP_KEY:
				for (let obstacle of this.levelService.levelInfo.obstaclePositions) {
					if (obstacle.x == this.levelService.levelInfo.currentPosition.x &&
						obstacle.y + 1 == this.levelService.levelInfo.currentPosition.y) {
						return true;
					}
				}
				return false;
	
				case CONSTANTS.RIGHT_KEY:
				for (let obstacle of this.levelService.levelInfo.obstaclePositions) {
					if (obstacle.y == this.levelService.levelInfo.currentPosition.y &&
						obstacle.x == this.levelService.levelInfo.currentPosition.x + 1) {
						return true;
					}
				}
				return false;
	
				case CONSTANTS.DOWN_KEY:
				for (let obstacle of this.levelService.levelInfo.obstaclePositions) {
					if (obstacle.x == this.levelService.levelInfo.currentPosition.x &&
						obstacle.y == this.levelService.levelInfo.currentPosition.y + 1) {
						return true;
					}
				}
				return false;
			}
		} else {
			return false;
		}
	}

	startUpMove() {
		if (this.levelService.levelInfo.currentPosition.y > 0 &&
			!this.isObstacle(CONSTANTS.UP_KEY)) {
			this.levelService.levelInfo.disableMove = CONSTANTS.UP_KEY;
			this.levelService.levelInfo.oldPosition = this.levelService.levelInfo.currentPosition.y;
			this.moveUp();
		}
	}

	startRightMove() {
		if (this.levelService.levelInfo.currentPosition.x < this.levelService.levelInfo.mapWidth - 1 &&
			!this.isObstacle(CONSTANTS.RIGHT_KEY)) {
			this.levelService.levelInfo.disableMove = CONSTANTS.RIGHT_KEY;
			this.levelService.levelInfo.oldPosition = this.levelService.levelInfo.currentPosition.x;
			this.moveRight();
		}
	}

	startDownMove() {
		if (this.levelService.levelInfo.currentPosition.y < this.levelService.levelInfo.mapHeight - 1 &&
			!this.isObstacle(CONSTANTS.DOWN_KEY)) {
			this.levelService.levelInfo.disableMove = CONSTANTS.DOWN_KEY;
			this.levelService.levelInfo.oldPosition = this.levelService.levelInfo.currentPosition.y;
			this.moveDown();
		}
	}

	doAction() {
		if (this.isObstacle(this.levelService.levelInfo.direction)) {
			this.levelService.levelInfo.disableMove = CONSTANTS.ACTION_KEY;
			this.drawService.drawMessage();
		}
	}
}
