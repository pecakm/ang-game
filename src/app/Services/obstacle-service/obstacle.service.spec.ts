import { TestBed, inject } from '@angular/core/testing';

import { ObstacleService } from './obstacle.service';

describe('ObstacleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ObstacleService]
    });
  });

  it('should be created', inject([ObstacleService], (service: ObstacleService) => {
    expect(service).toBeTruthy();
  }));
});
