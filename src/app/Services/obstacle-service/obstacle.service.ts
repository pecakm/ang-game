import { Injectable } from '@angular/core';
import { CONSTANTS } from '../../helpers/Constants';
import { Position } from '../../Models/Position';
import { LevelInfo } from '../../Models/LevelInfo';
import { LevelService } from '../level-service/level.service';

@Injectable({
	providedIn: 'root'
})
export class ObstacleService {
	private obstacleOldPosition = 0;
	public movingObstacle: Position;
	public obstacleDirection = 0;

	constructor(
		private levelService: LevelService
	) { }

	continueObstacleMove() {
		switch (this.obstacleDirection) {
			case CONSTANTS.LEFT_KEY:
				this.moveObstacleLeft(this.levelService.levelInfo);
				break;

			case CONSTANTS.UP_KEY:
				this.moveObstacleUp(this.levelService.levelInfo);
				break;

			case CONSTANTS.RIGHT_KEY:
				this.moveObstacleRight(this.levelService.levelInfo);
				break;

			case CONSTANTS.DOWN_KEY:
				this.moveObstacleDown(this.levelService.levelInfo);
				break;
		}
	}

	moveObstacleLeft(levelInfo: LevelInfo) {
		this.movingObstacle.x = Math.round((this.movingObstacle.x - CONSTANTS.SPEED) * 100) / 100;

    	if (this.obstacleOldPosition - this.movingObstacle.x >= 1) {
        	this.movingObstacle.x = this.obstacleOldPosition - 1;
        	this.finishObstacleMove(levelInfo);
    	}
	}

	finishObstacleMove(levelInfo: LevelInfo) {
		levelInfo.disableMove = 0;
		this.obstacleOldPosition = 0;
		this.obstacleDirection = 0;
		this.movingObstacle = null;
	}

	moveObstacleUp(levelInfo: LevelInfo) {
		this.movingObstacle.y = Math.round((this.movingObstacle.y - CONSTANTS.SPEED) * 100) / 100;

    	if (this.obstacleOldPosition - this.movingObstacle.y >= 1) {
        	this.movingObstacle.y = this.obstacleOldPosition - 1;
        	this.finishObstacleMove(levelInfo);
    	}
	}

	moveObstacleRight(levelInfo: LevelInfo) {
		this.movingObstacle.x = Math.round((this.movingObstacle.x + CONSTANTS.SPEED) * 100) / 100;

    	if (this.movingObstacle.x - this.obstacleOldPosition >= 1) {
        	this.movingObstacle.x = this.obstacleOldPosition + 1;
        	this.finishObstacleMove(levelInfo);
    	}
	}

	moveObstacleDown(levelInfo: LevelInfo) {
		this.movingObstacle.y = Math.round((this.movingObstacle.y + CONSTANTS.SPEED) * 100) / 100;

    	if (this.movingObstacle.y - this.obstacleOldPosition >= 1) {
        	this.movingObstacle.y = this.obstacleOldPosition + 1;
        	this.finishObstacleMove(levelInfo);
    	}
	}

	moveObstacle(levelInfo: LevelInfo) {
		this.obstacleDirection = levelInfo.direction;
		switch (levelInfo.direction) {
			case CONSTANTS.LEFT_KEY:
			this.movingObstacle = levelInfo.obstaclePositions.find(
				obstacle => obstacle.x + 1 == levelInfo.currentPosition.x &&
				obstacle.y == levelInfo.currentPosition.y);
			this.obstacleOldPosition = this.movingObstacle.x;
			this.moveObstacleLeft(levelInfo);
			break;

			case CONSTANTS.UP_KEY:
			this.movingObstacle = levelInfo.obstaclePositions.find(
				obstacle => obstacle.y + 1 == levelInfo.currentPosition.y &&
				obstacle.x == levelInfo.currentPosition.x);
			this.obstacleOldPosition = this.movingObstacle.y;
			this.moveObstacleUp(levelInfo);
			break;

			case CONSTANTS.RIGHT_KEY:
			this.movingObstacle = levelInfo.obstaclePositions.find(
				obstacle => obstacle.x == levelInfo.currentPosition.x + 1 &&
				obstacle.y == levelInfo.currentPosition.y);
			this.obstacleOldPosition = this.movingObstacle.x;
			this.moveObstacleRight(levelInfo);
			break;

			case CONSTANTS.DOWN_KEY:
			this.movingObstacle = levelInfo.obstaclePositions.find(
				obstacle => obstacle.y == levelInfo.currentPosition.y + 1 &&
				obstacle.x == levelInfo.currentPosition.x);
			this.obstacleOldPosition = this.movingObstacle.y;
			this.moveObstacleDown(levelInfo);
			break;
		}
	}
}
