import { Position } from './Position';

export class LevelInfo {
    level: number;
    mapHeight: number;
    mapWidth: number;
    oldPosition: number;
    direction: number;
    obstaclePositions: Position[];
    exitPosition: Position;
    currentPosition: Position;
    disableMove: number;
}