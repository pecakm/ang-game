import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CONSTANTS } from '../helpers/Constants';
import { LevelService } from '../Services/level-service/level.service';
import { DrawService } from '../Services/draw-service/draw.service';
import { ObstacleService } from '../Services/obstacle-service/obstacle.service';
import { MoveService } from '../Services/move-service/move.service';

@Component({
	selector: 'app-game-window',
	templateUrl: './game-window.component.html',
	styleUrls: ['./game-window.component.css'],
	host: {
		'(document:keypress)': 'move($event)'
	}
})
export class GameWindowComponent implements OnInit {
	@ViewChild('canvas') canvas: ElementRef;
	cx: CanvasRenderingContext2D;

	constructor(
		private levelService: LevelService,
		private drawService: DrawService,
		private obstacleService: ObstacleService,
		private moveService: MoveService
	) { }

	ngOnInit() {
		this.levelService.loadLevelInfo(0);
		const canvasEl: HTMLCanvasElement = this.canvas.nativeElement;
		this.drawService.cx = canvasEl.getContext('2d');

		canvasEl.width = CONSTANTS.CANVAS_SIZE;
		canvasEl.height = CONSTANTS.CANVAS_SIZE;

		setInterval(() => {
			this.drawService.drawView();

			if (this.levelService.getDisableMove() != 0) {
				this.continueAnimation();
			}
		}, 50);
	}

	continueAnimation() {
		if (this.levelService.getDisableMove() == CONSTANTS.ACTION_KEY) {
			this.drawService.drawMessage();

			if (this.obstacleService.movingObstacle) {
				this.obstacleService.continueObstacleMove();
			}
		} else {
			this.moveService.continueMove();
			this.levelService.checkExit();
		}
	}

	move(event: KeyboardEvent) {
		if (this.levelService.getDisableMove() == 0) {
			if (event.keyCode == CONSTANTS.LEFT_KEY || 
				event.keyCode == CONSTANTS.UP_KEY || 
				event.keyCode == CONSTANTS.RIGHT_KEY ||
				event.keyCode == CONSTANTS.DOWN_KEY) {
				this.levelService.levelInfo.direction = event.keyCode;
			}
			
			switch (event.keyCode) {
				case CONSTANTS.LEFT_KEY:
				this.moveService.startLeftMove();
				break;
		
				case CONSTANTS.UP_KEY:
				this.moveService.startUpMove();
				break;
	
				case CONSTANTS.RIGHT_KEY:
				this.moveService.startRightMove();
				break;
	
				case CONSTANTS.DOWN_KEY:
				this.moveService.startDownMove();
				break;

				case CONSTANTS.ACTION_KEY:
				this.moveService.doAction();
				break;
			}
		} else if (this.levelService.levelInfo.disableMove == CONSTANTS.ACTION_KEY && event.keyCode == CONSTANTS.ACTION_KEY) {
			this.obstacleService.moveObstacle(this.levelService.levelInfo);
		}
	}
}
